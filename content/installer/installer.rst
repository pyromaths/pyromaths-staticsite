:title: Installer Pyromaths
:category: installer
:date: 2018-07-05 14:32
:tags: hidden
:description: Installer Pyromaths

Installer Pyromaths sur Linux, Mac OS X ou Windows
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour en savoir plus sur l'installation de Pyromaths, cliquez sur le
système d'exploitation qui vous intéresse.

.. raw:: html

  <div id="trois_cols">
  <div class="centerleft">

|Pypi|
`Installation de Pyromaths depuis Pypi <{filename}/installer/installer_pypi.rst>`__

.. raw:: html

  </div>
  <div class="centerleft">

|Linux|
`Installation de Pyromaths sur Linux <installer-pyromaths-sous-linux.html>`__

.. raw:: html

  </div>
  <div class="centerright">


|Windows|
`Installation de Pyromaths sous Windows <installer-pyromaths-sous-windows.html>`__

.. raw:: html

  </div>
  <div class="center">

|Mac OS X|
`Installation de Pyromaths sur Mac OS X <installer-pyromaths-sur-macos.html>`__

.. raw:: html

  </div>
  </div>

Utiliser, comprendre, contribuer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour tout savoir sur Pyromaths, nous vous invitons à parcourir la
documentation ci-contre.

.. |Linux| image:: images/linux.png
  :target: installer-la-version-linux
.. |Windows| image:: images/winvista.png
  :target: installer-la-version-windows
.. |Pypi| image:: images/source.png
  :target: installer-la-version-pypi
.. |Mac OS X| image:: images/macosx.png
  :target: installer-la-version-mac-os-x
