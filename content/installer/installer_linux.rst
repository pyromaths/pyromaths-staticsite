:title: Installer Pyromaths sous Linux
:date: 2009-09-15 12:00
:modified: 2018-07-04 19:52
:category: installer
:authors: Jérôme
:description: Installer Pyromaths sous Linux

.. image:: images/pyromaths-linux.png
    :alt: Linux
    :width: 611px

Pour Ubuntu
===========

Il est conseillé d'ajouter le dépôt de Launchpad :

.. class:: console

    .. code-block:: bash
      :linenos: none

      sudo add-apt-repository ppa:jerome-ortais/ppa

Puis télécharger la clef gpg :

.. class:: console

    .. code-block:: bash
      :linenos: none

      sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 9B245E3CCC82CBA7

Mettre à jour la base de données des paquets :

.. class:: console

    .. code-block:: bash
      :linenos: none

      sudo apt-get update

Puis il suffit d'installer le paquet `pyromaths <apt://pyromaths>`_.

**Note:** *la version de pyromaths se mettra automatiquement à jour à l'avenir.*

Vous retrouverez Pyromaths dans le menu *Applications / Éducation*.

Installation de LaTeX minimale
==============================

Si la distribution LaTeX vous paraît trop volumineuse, vous pouvez installer une version  allégée d'environ 300 Mo.

La documetnation se trouve `ici <une-distribution-latex-allegee-linux.html>`__.

RPM et DEB
==========

Pour les distributions utilisant le format RPM ou DEB, téléchargez le fichier qui vous convient dans la rubrique
**Télécharger** ci-dessus.
Il vous faudra ensuite consulter régulièrement le site pour vous tenir au courant des mises à jour.

Les dépendances sont les suivantes :

* python3-qt5,
* python3-lxml,
* texlive-pstricks,
* texlive-latex-base,
* texlive-latex-extra,
* texlive-fonts-recommended,
* texlive-latex-recommended.
* texlive-science

Vous retrouverez Pyromaths dans le menu *Applications / Éducation*.

Et les autres ?
===============

Les autres devront utiliser directement les sources.

Si vous avez des difficultés à installer ou à faire fonctionner Pyromaths, rendez-vous sur le `Forum <https://forum.pyromaths.org>`_.
Nous tenterons de vous répondre dans les meilleurs délais.
