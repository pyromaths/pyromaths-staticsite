:title: Installer Pyromaths sur macOS
:date: 2009-09-15 12:00
:category: installer
:authors: Yves
:description: Installer Pyromaths sur macOS


.. image:: images/pyromaths_mac.png
    :alt: Mac OS X
    :align: center

Configuration requise
=====================


* Ordinateur Mac avec OS X 10.10 ou ultérieur.
* Une distribution LaTeX. Nous vous conseillons `MacTeX <http://www.tug.org/mactex/>`__: téléchargez et installez «MacTeX.pkg».
  
  Le package Tkz-Tab (tableaux de signes et de variations), nécessaire au fonctionnement de Pyromaths, n'est plus inclus dans la distribution MacTeX-2020. Pour l'installer, ouvrez l'application Terminal et exécutez la commande suivante:

  .. class:: console

    .. code-block:: bash
      :linenos: none

      sudo tlmgr install tkz-tab

Installation de Pyromaths
=========================

*  Téléchargez la version macOS de Pyromaths dans la rubrique **Télécharger** ci-dessus.
*  Glisser-déposer *Pyromaths* dans le dossier *Applications*.

