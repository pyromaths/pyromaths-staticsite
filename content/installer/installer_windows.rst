:title: Installer Pyromaths sous Windows
:date: 2015-03-27 12:00
:modified: 2021-08-24 09:00
:category: installer
:authors: Jérôme
:description: Installer Pyromaths sous Windows

|windows|

Installation de Pyromaths
=========================

Téléchargez la version Windows de Pyromaths dans la rubrique **Télécharger** ci-dessus et lancez l'installateur.

Il vous faudra ensuite installer deux programmes nécessaires à l'utilisation de Pyromaths:

-  TeXLive qui permet de compiler les fichier LaTeX créés par Pyromaths (c'est-à-dire de les transformer en fichiers pdf).
-  Sumatra PDF Viewer qui permet de lire et d'imprimer ces fichiers.

Installation de la distribution TeXLive
=======================================

Je vous conseille de télécharger le fichier *install-tl-windows.exe* à partir du 
`site de TeXLive <https://www.tug.org/texlive/acquire-netinstall.html>`__ et de l'exécuter.

Pour une installation minimale de TeXLive, vous choisirez « custom install ».

|Installation TeXLive étape 1|

Validez en cliquant sur « Install ».

|Installation TeXLive étape 2|

Sélectionnez « Avancé ».

|Installation TeXLive étape 2 bis|

Choisissez « schéma minimal (Plain seulement) » dans l'option « Schéma sélectionné ».

|Installation TeXLive étape 3|

Désactivez « Installer la documentation des macros et polices » ainsi que « Installer les sources des macros et polices ».

|Installation TeXLive étape 4|

Il vous faudra ensuite installer les paquets supplémentaires requis.
Pour cela, ouvrez une « invite de commandes» à partir du menu *Démarrer* et saisissez la ligne suivante :

.. class:: console

  .. code-block:: bash
    :linenos: none

    c:\texlive\2021\bin\win32\tlmgr.bat install adjustbox amsmath asymptote \
    avantgar babel babel-french cancel carlisle cm-super collectbox colortbl \
    enumitem fancyhdr geometry graphics hyperref interval latex-bin latexmk \
    l3kernel l3packages lm marvosym mathtools ms multido numprint oberdiek pgf \
    ps2eps psnfss pst-3d pst-arrow pst-calculate pst-eucl pst-math pst-node \
    pst-plot pst-tools pstool pstricks pstricks-add siunitx tkz-tab tools units \
    wasy wasysym wasy-type1 wrapfig xkeyval

**Remarque :** si votre utilisateur n'a pas les droits administrateur,dans la ligne de commande vous devrez saisir :

.. class:: console

  .. code-block:: bash
    :linenos: none

    RunAs.exe /user:Administrator "c:\texlive\2021\bin\win32\tlmgr.bat install \
    adjustbox amsmath asymptote avantgar babel babel-french cancel carlisle \
    cm-super collectbox colortbl enumitem fancyhdr geometry graphics hyperref \
    interval latex-bin latexmk l3kernel l3packages lm marvosym mathtools ms \
    multido numprint oberdiek pgf ps2eps psnfss pst-3d pst-arrow pst-calculate \
    pst-eucl pst-math pst-node pst-plot pst-tools pstool pstricks pstricks-add \
    siunitx tkz-tab tools units wasy wasysym wasy-type1 wrapfig xkeyval"

|Dossier texlive|

Paquetages supplémentaires nécessaires à la compilation des documents créés par Pyromaths 18.9.2:
------------------------------------------------------------------------------------------------------

-  adjustbox
-  amsmath
-  asymptote
-  avantgar
-  babel
-  babel-french
-  cancel
-  carlisle
-  cm-super
-  collectbox
-  colortbl
-  enumitem
-  fancyhdr
-  geometry
-  graphics
-  hyperref
-  interval
-  latex-bin
-  latexmk
-  l3kernel
-  l3packages
-  lm
-  marvosym
-  mathtools
-  ms
-  multido
-  numprint
-  oberdiek
-  pgf
-  ps2eps
-  psnfss
-  pst-3d
-  pst-arrow
-  pst-calculate
-  pst-eucl
-  pst-math
-  pst-node
-  pst-plot
-  pst-tools
-  pstool
-  pstricks
-  pstricks-add
-  siunitx
-  tkz-tab
-  tools
-  units
-  wasy
-  wasysym
-  wasy-type1
-  wrapfig
-  xkeyval

Installation de Sumatra PDF viewer
==================================

Sumatra PDF Viewer est un lecteur de fichiers pdf pour Windows libre et gratuit sous licence gpl.
Il a moins de fonctionnalités qu'Adobe Reader (qui est un logiciel gratuit mais propriétaire), mais si vous voulez un
programme qui s'ouvre dans la seconde et qui ne fasse que lire les fichiers PDF, il est fait pour vous.
Vous trouverez la dernière version de ce programme sur la page
`Download <http://blog.kowalczyk.info/software/sumatrapdf/download.html>`__.
Il vous suffit de cliquer sur le lien qui suit *Installer* et d'exécuter le programme pour l'installer en validant toutes les étapes.

Si vous avez des difficultés à installer ou à faire fonctionner Pyromaths, rendez-vous sur le `Forum <https://forum.pyromaths.org>`__.
Nous tenterons de vous répondre dans les meilleurs délais.

.. |windows| image:: images/pyromaths_windows.png
.. |Installation TeXLive étape 1| image:: images/texlive/texlive2021-etape1.png
.. |Installation TeXLive étape 2| image:: images/texlive/texlive2021-etape2.png
.. |Installation TeXLive étape 2 bis| image:: images/texlive/texlive2021-etape2bis.png
.. |Installation TeXLive étape 3| image:: images/texlive/texlive2021-etape3.png
.. |Installation TeXLive étape 4| image:: images/texlive/texlive2021-etape4.png
.. |Dossier texlive| image:: images/texlive/texlive2021-etape5.png
