:title: Une distribution LaTeX allégée pour OS X
:date: 2013-11-20 12:00
:update: 2018-12-03- 16:54
:category: installer
:authors: Yves
:description: Une distribution LaTeX allégée pour OS X

Cet article est destiné aux utilisateurs avancés de OS X, soucieux d’économiser de l'espace disque,
qui souhaitent installer BasicTeX, une distribution LaTeX plus légère que MacTeX.

**Avertissement:** *La procédure implique d’être à l’aise avec le Terminal pour lancer des lignes de commandes en mode super utilisateur.*

Installation de BasicTeX
========================

`Télécharger <http://tug.org/mactex/morepackages.html>`__ et installer **BasicTeX**.

Installation de Ghostscript pour obtenir ps2pdf
===============================================

Pour installer l’utilitaire ps2pdf, `télécharger <http://pages.uoregon.edu/koch/>`__ et installer **Ghostscript**.

**Remarque:** Il est également possible de `télécharger <http://tug.org/mactex/morepackages.html>`__ Ghostscript from MacTeX.

Installation des packages nécessaires à Pyromaths
=================================================

Dans le Terminal, saisir les lignes de commande suivantes afin de mettre à jour TEX Live Manager et d’installer les
packages nécessaires à Pyromaths :

.. class:: console

  .. code-block:: bash
    :linenos: none

    sudo /usr/local/texlive/2018basic/bin/x86_64-darwin/tlmgr update --self
    sudo /usr/local/texlive/2018basic/bin/x86_64-darwin/tlmgr install adjustbox \
    amsfonts amsmath asymptote avantgar babel babel-french cancel carlisle collectbox \
    collection-fontsrecommended colortbl ec enumitem fancyhdr geometry graphics \
    hyperref iftex interval latex-bin latexmk lm marvosym mathtools ms multido \
    numprint oberdiek pgf psnfss pst-3d pst-arrow pst-calculate pst-eucl pst-math \
    pst-node pst-plot pst-tools pstools pstricks pstricks-add siunitx tkz-tab tools \
    units url wasy wasy2-ps wasysym wrapfig xcolor xkeyval

Remarque: Si une fenêtre apparaît demandant d’installer les outils en ligne de commande Xcode,
il suffit de cliquer sur le bouton Plus tard car cette installation n’est pas nécessaire.
