:title: Une distribution LaTeX allégée Linux
:date: 2018-07-08 13:08
:modified: 2021-08-24 11:27
:category: installer
:authors: Jérôme
:description: Une distribution LaTeX allégée pour Linux Debian/Ubuntu

Cet article est destiné aux utilisateurs avancés de GNU/Linux, soucieux d’économiser de l’espace disque,
qui souhaitent installer une distribution LaTeX plus légère que celle fournie par les dépendances de Pyromaths.

**Avertissement:** *La procédure implique d’être à l’aise avec le Terminal pour lancer des lignes de commandes en mode super utilisateur.*

Suppression de la version deb de TeXLive
========================================

Il est nécessaire de supprimer la version existente de TeXLive que vous auriez pu installer.

.. class:: console

  .. code-block:: bash
    :linenos: none

    sudo apt --auto-remove purge chktex cm-super cm-super-minimal context \
    dvidvi dvipng feynmf fragmaster lacheck latex-cjk-all latex-cjk-chinese \
    latex-cjk-chinese-arphic-bkai00mp latex-cjk-chinese-arphic-bsmi00lp \
    latex-cjk-chinese-arphic-gbsn00lp latex-cjk-chinese-arphic-gkai00mp \
    latex-cjk-common latex-cjk-japanese latex-cjk-japanese-wadalab \
    latex-cjk-korean latex-cjk-thai latexdiff latexmk lcdf-typetools lmodern \
    preview-latex-style ps2eps psutils purifyeps t1utils tex-gyre texlive \
    texlive-base texlive-bibtex-extra texlive-binaries texlive-extra-utils \
    texlive-fonts-extra texlive-fonts-extra-doc texlive-fonts-recommended \
    texlive-fonts-recommended-doc texlive-font-utils texlive-formats-extra \
    texlive-games texlive-humanities texlive-humanities-doc texlive-lang-all \
    texlive-lang-arabic texlive-lang-cjk texlive-lang-cyrillic \
    texlive-lang-czechslovak texlive-lang-english texlive-lang-european \
    texlive-lang-japanese texlive-lang-chinese texlive-lang-korean \
    texlive-lang-french texlive-lang-german texlive-lang-greek \
    texlive-lang-italian texlive-lang-other texlive-lang-polish \
    texlive-lang-portuguese texlive-lang-spanish texlive-latex-base \
    texlive-latex-base-doc texlive-latex-extra texlive-latex-extra-doc \
    texlive-latex-recommended texlive-latex-recommended-doc texlive-luatex \
    texlive-metapost texlive-metapost-doc texlive-music texlive-pictures \
    texlive-pictures-doc texlive-plain-generic texlive-pstricks \
    texlive-pstricks-doc texlive-publishers texlive-publishers-doc \
    texlive-science texlive-science-doc texlive-xetex tipa tipa-doc xindy \
    xindy-rules asymptote texinfo

Installation d'un "fake package"
================================

Version simple
--------------

Télécharger le `paquet <images/texlive-linux/texlive-local_2021.99999999-1_all.deb>`__ que j'ai créé et l'installer.

.. class:: console

  .. code-block:: bash
    :linenos: none

    sudo dpkg -i texlive-local_2021.99999999-1_all.deb
    sudo apt install -f

Version manuelle
----------------


.. class:: console

  .. code-block:: bash
    :linenos: none

    sudo apt install equivs --no-install-recommends
    mkdir -p /tmp/tl-equivs && cd /tmp/tl-equivs
    wget https://www.tug.org/texlive/files/debian-equivs-2021-ex.txt -O texlive-local
    equivs-build texlive-local
    sudo dpkg -i texlive-local_2021.99999999-1_all.deb
    sudo apt install -f


Installation de la dernière version de teXLive
==============================================

Installer le nécessaire à l'installation :

.. class:: console

  .. code-block:: bash
    :linenos: none

    sudo apt install perl-tk
    cd /tmp
    wget http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz
    tar -vxzf install-tl-unx.tar.gz
    cd install-tl-2021????
    sudo ./install-tl -gui

|Installation TeXLive étape 1|

Pour une installation minimale de TeXLive, vous choisirez «Avancé » puis vous
modifierez le Schéma pour choisir « Schéma minimal (Plain seulement) ».

|Installation TeXLive étape 2|

Désactivez « Installer la documentation des macros et polices » ainsi que « Installer les sources des macros et polices ».

|Installation TeXLive étape 3|

Ensuite, il faut intégrer les chemins dans */etc/profile.d/texlive.sh*

.. class:: code

  .. code-block:: bash
    :linenos: none

    MANPATH=/usr/local/texlive/2021/texmf-dist/doc/man:$MANPATH
    INFOPATH=/usr/local/texlive/2021/texmf-dist/doc/info:$INFOPATH
    PATH=/usr/local/texlive/2021/bin/x86_64-linux:$PATH

Il vous faut ensuite installer les paquets supplémentaires requis.

.. class:: console

  .. code-block:: bash
    :linenos: none

    source /etc/profile.d/texlive.sh
    tlmgr install adjustbox amsmath asymptote avantgar babel babel-french \
    cancel carlisle cm-super collectbox colortbl enumitem fancyhdr geometry \
    graphics hyperref interval latex-bin latexmk l3kernel l3packages lm \
    marvosym mathtools ms multido numprint oberdiek pgf ps2eps psnfss pst-3d \
    pst-arrow pst-calculate pst-eucl pst-math pst-node pst-plot pst-tools \
    pstool pstricks pstricks-add siunitx tkz-tab tools units wasy wasysym \
    wasy-type1 wrapfig xkeyval


.. |Installation TeXLive étape 1| image:: images/texlive-linux/texlive1.png
.. |Installation TeXLive étape 2| image:: images/texlive-linux/texlive2.png
.. |Installation TeXLive étape 3| image:: images/texlive-linux/texlive3.png
