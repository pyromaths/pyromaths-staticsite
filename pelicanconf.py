#!/usr/bin/env python3

AUTHOR = 'Jérôme'
SITENAME = 'Pyromaths'
SITEURL = 'https://www.pyromaths.org'

# can be useful in development, BUT SET TO FALSE WHEN YOU'RE READY TO PUBLISH
RELATIVE_URLS = True

USE_FOLDER_AS_CATEGORY = True
DEFAULT_CATEGORY = 'telecharger'

PATH = 'content'

# static paths will be copied without parsing their contents
STATIC_PATHS = [
    'images',
    '/files/js',
    '/files/styles',
    'extra/robots.txt'
]

EXTRA_PATH_METADATA = {
    'extra/robots.txt': {'path': 'robots.txt'},
    }
TIMEZONE = 'Europe/Paris'
DEFAULT_DATE_FORMAT = '%d %b %y'

DEFAULT_LANG = 'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
#SOCIAL = ((),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
DELETE_OUTPUT_DIRECTORY = True
OUTPUT_RETENTION = [".git"]
OUTPUT_PATH = "public"

TYPOGRIFY = True

JINJA_ENVIRONMENT = {'trim_blocks': True, 'lstrip_blocks': True}

# code blocks with line numbers
PYGMENTS_RST_OPTIONS = {'linenos': 'table'}

THEME='theme'
THEME_STATIC_DIR = 'theme'
THEME_STATIC_PATHS = ['static']
CSS_FILE = 'style.css'

PLUGIN_PATHS = ['pelican-plugins']
PLUGINS = ['sitemap' ]

SITEMAP = {
    'format': 'xml',
    'exclude': ['tags.html', 'categories.html', 'tag/', 'author'],
}

AUTHOR_SAVE_AS = ''
AUTHORS_SAVE_AS = ''
CATEGORY_SAVE_AS = ''
CATEGORIES_SAVE_AS = ''
TAG_SAVE_AS = ''
TAGS_SAVE_AS = ''
